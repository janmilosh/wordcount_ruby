require_relative '../word_count.rb'

describe "Prepare text" do

  let(:word_count) { WordCount.new }
  let(:filename) { "spec/data/test_data.txt" }
  let(:file_without_gutenberg_text) { "\n\n\n\ncan't \"Geneva, March 18th, 17--.\"\n\n\n\"Dear, (dear) Elizabeth!\" _I_ exclaimed, 'when' I had read her letter:  \"I\nwill write instantly and heart-sickeningly roses,--in relieve them from the anxiety they must feel-\ning?\"\n\n\n\n\n\n" }
  let(:initial_array) { ["can't", "\"geneva,", "march", "18th,", "17", ".\"", "\"dear,", "(dear)", "elizabeth!\"", "i", "exclaimed,", "'when'", "i", "had", "read", "her", "letter:", "\"i", "will", "write", "instantly", "and", "heart-sickeningly", "roses,", "in", "relieve", "them", "from", "the", "anxiety", "they", "must", "feeling?\""] }
  let(:no_numbers_array) { ["can't", "\"geneva,", "march", ".\"", "\"dear,", "(dear)", "elizabeth!\"", "i", "exclaimed,", "'when'", "i", "had", "read", "her", "letter:", "\"i", "will", "write", "instantly", "and", "heart-sickeningly", "roses,", "in", "relieve", "them", "from", "the", "anxiety", "they", "must", "feeling?\""] }
  let(:no_punctuation_array) { ["can't", "geneva", "march", "", "dear", "dear", "elizabeth", "i", "exclaimed", "when", "i", "had", "read", "her", "letter", "i", "will", "write", "instantly", "and", "heart-sickeningly", "roses", "in", "relieve", "them", "from", "the", "anxiety", "they", "must", "feeling"] }
  let(:word_array) { ["can't", "geneva", "march", "dear", "dear", "elizabeth", "i", "exclaimed", "when", "i", "had", "read", "her", "letter", "i", "will", "write", "instantly", "and", "heart-sickeningly", "roses", "in", "relieve", "them", "from", "the", "anxiety", "they", "must", "feeling"] }
  let(:word_hash) { {"can't"=>1, "geneva"=>1, "march"=>1, "dear"=>2, "elizabeth"=>1, "i"=>3, "exclaimed"=>1, "when"=>1, "had"=>1, "read"=>1, "her"=>1, "letter"=>1, "will"=>1, "write"=>1, "instantly"=>1, "and"=>1, "heart-sickeningly"=>1, "roses"=>1, "in"=>1, "relieve"=>1, "them"=>1, "from"=>1, "the"=>1, "anxiety"=>1, "they"=>1, "must"=>1, "feeling"=>1} }
  let(:counted_word_array) { [["i", 3], ["dear", 2], ["write", 1], ["anxiety", 1], ["the", 1], ["from", 1], ["them", 1], ["relieve", 1], ["in", 1], ["roses", 1], ["heart-sickeningly", 1], ["and", 1], ["instantly", 1], ["they", 1], ["will", 1], ["letter", 1], ["her", 1], ["read", 1], ["had", 1], ["when", 1], ["exclaimed", 1], ["must", 1], ["elizabeth", 1], ["feeling", 1], ["march", 1], ["geneva", 1], ["can't", 1]] }
  let(:total_words) { 27 }

  it "reads the word file except for the gutenberg text" do
    text = word_count.read_file(filename)
    expect(text).to eq file_without_gutenberg_text
  end

  it "creates an array of lowercase words" do
    arr = word_count.initial_word_array(file_without_gutenberg_text)
    expect(arr).to eq initial_array
  end

  it "removes words with numbers" do
    arr = word_count.remove_number_words(initial_array)
    expect(arr).to eq no_numbers_array
  end

  it "removes punctuation from words" do
    arr = word_count.remove_punctuation!(no_numbers_array)
    expect(arr).to eq no_punctuation_array
  end

  it "doesn't remove accented non-english letters" do
    arr = ["jörg", "eieieiiiinöööööiöööiöööiiööööi"]
    new_arr = ["jörg", "eieieiiiinöööööiöööiöööiiööööi"]
    word_count.remove_punctuation!(arr)
    expect(arr).to eq new_arr
  end

  it "removes empty strings from word array" do
    arr = word_count.remove_empty_strings(no_punctuation_array)
    expect(arr).to eq word_array
  end

  it "removes single quotes from a word" do
    word = "'something'"
    contraction = "wouldn't"
    new_word = word_count.remove_single_quotes(word)
    expect(new_word).to eq "something"
    new_contraction = word_count.remove_single_quotes(contraction)
    expect(new_contraction).to eq "wouldn't"
  end

  it "counts the words and stores results in a hash" do
    results = word_count.count(word_array)
    expect(results).to eq word_hash
  end

  it "orders the words in an array by count, high to low" do
    ordered_array = word_count.order_array(word_hash)
    expect(ordered_array).to eq counted_word_array
    expect(ordered_array.size).to eq total_words
  end

  it "validates negative input" do
    validation = word_count.valid?("-5", total_words)
    expect(validation).to eq false
  end

  it "validates zero input" do
    validation = word_count.valid?("0", total_words)
    expect(validation).to eq false
  end

  it "validates valid input" do
    validation = word_count.valid?(total_words, total_words)
    expect(validation).to eq true
  end

  it "validates too large number input" do
    validation = word_count.valid?((total_words + 1).to_s, total_words)
    expect(validation).to eq false
  end

  it "validates non-digit characters" do
    validation = word_count.valid?("1aa", total_words)
    expect(validation).to eq false
  end
    
  it "validates non-integer inputs" do
    validation = word_count.valid?("1.55", total_words)
    expect(validation).to eq false
  end

  it "calculates the bar width for graphing" do
    width = word_count.bar_width(4194, 1033)
    expect(width).to eq 22
  end 
end 

require_relative "word_count"

word_count = WordCount.new

# filename = 'from_the_earth_to_the_moon.txt'
# filename = 'frankenstein.txt'
filename = ARGV[0]
gutenberg_free_text = word_count.read_file(filename)
initial_array = word_count.initial_word_array(gutenberg_free_text)
no_numbers_array = word_count.remove_number_words(initial_array)
no_punctuation_array = word_count.remove_punctuation!(no_numbers_array)
word_array = word_count.remove_empty_strings(no_punctuation_array)
word_hash = word_count.count(word_array)
ordered_array = word_count.order_array(word_hash)
word_count.print_results(ordered_array, filename)

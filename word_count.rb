class WordCount
    
  def read_file(filename)
    text = ""
    file = File.open(filename)
    file.each do |line|
      if line.include?('START OF THIS PROJECT GUTENBERG')
        break
      end
    end
    file.each do |line|
      if line.include?('Gutenberg') and line.include?('End of')
        break
      end
      text << line
    end
    file.close
    text
  end

  def initial_word_array(str)
    str.downcase.gsub(/-\n/, "").gsub(/_/, "").gsub(/--/, " ").gsub(/\s+/m, " ").gsub(/^\s+|\s+$/m, "").split(" ")
  end

  def remove_number_words(word_array)
    word_array.delete_if {|word| word =~ /\d/ }
  end

  def remove_punctuation!(word_array)
    word_array.each do |word|
      word.gsub!(/[^\p{L}'-]/, "")
      self.remove_single_quotes(word)
    end
  end

  def remove_single_quotes(word)
    word.chomp!("'")
    word.reverse!
    word.chomp!("'")
    word.reverse!
  end

  def remove_empty_strings(word_array)
    word_array.reject { |w|  w == "" }
  end

  def count(word_array)
    word_hash = {}
    word_array.each do |word|
      word_hash[word] ? word_hash[word] += 1: word_hash[word] = 1
    end
    word_hash
  end

  def order_array(word_hash)
    word_hash.to_a.sort_by { |word| word[1]}.reverse!
  end

  def valid?(input, total_words)
    return false if input =~ /\D/
    return true if (0 < input.to_i and input.to_i <= total_words)
    return false
  end

  def bar_width(most_occurrences, occurrences)
    (89.0 * occurrences/most_occurrences).round
  end

  def comma_separated(number)
    number.to_s.chars.to_a.reverse.each_slice(3).map(&:join).join(",").reverse
  end

  def num_words_input(total_words)
    print "How many of them would you like to display? "
    input = $stdin.gets.chomp
    if valid?(input, total_words)
      return input
    else
      puts "\nPlease input an integer between 1 and #{total_words}."
      num_words_input(total_words)
    end
  end

  def print_results(ordered_array, filename)
    total_words = ordered_array.inject(0){|sum, item| sum + item[1]}
    total_unique_words = ordered_array.size

    most_occurrences = ordered_array[0][1]
    puts "\n\n\n#{"*-"*50}*\n#{"-*"*50}-\n\n\n"
    puts "There are #{ comma_separated(total_unique_words) } unique words out of #{ comma_separated(total_words) } total words in file #{ filename }."
    num_words = num_words_input(total_unique_words).to_i
    selected_words = ordered_array.first(num_words)
    puts "\nShowing the top #{ comma_separated(num_words) } out of #{ comma_separated(total_unique_words) } unique words. Excludes Gutenberg text at top and bottom and words that\ncontain numbers. Hyphenated words and contractions remain as written and all text is converted to\nlowercase. Punctuation and special characters are removed.\n\n"
    selected_words.each do |word|
      width = self.bar_width(most_occurrences, word[1])
      puts "#{ "="*width } #{ word[0] } (#{ comma_separated(word[1]) })" 
    end
    puts "\n\n#{ "*-"*50 }*\n#{ "-*"*50 }-\n\n\n"
  end
end

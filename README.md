# Word Count Exercise

From COhPy meetup, but done in Ruby.

This program will count and sort the unique words in a Gutenberg book (high to low) and display them in a graph on the command line.

The Gutenberg text at the top and bottom and words that contain numbers are excluded. Hyphenated words and contractions remain as written and all text is converted to lowercase. Punctuation and special characters are removed.

Run from the command line and give input when prompted:

```
$ ruby run_word_count.rb frankenstein.txt
```
or
```
$ ruby run_word_count.rb from_the_earth_to_the_moon.txt
```
or
```
ruby run_word_count.rb sekunde_durch_hirn.txt
```

To run the tests, you will need to have ```bundler``` installed.

```
$ bundle install
$ bundle exec rspec spec
```
